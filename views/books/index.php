<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $aBooks  */
/* @var $aAuthors  */
/* @var $aUrlToSort  */
/* @var $sOrderBy  */
/* @var $sOrder  */

$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile("/web/js/lightbox/ekko-lightbox.min.css", [
    'depends' => [yii\web\JqueryAsset::className()],
    'media' => 'print',
], 'css-print-theme');
$this->registerCssFile("/web/js/datepicker/datepicker.css");
$this->registerJsFile(
    '/web/js/lightbox/ekko-lightbox.min.js',
    ['depends' => [yii\web\JqueryAsset::className()]]
);
$this->registerJsFile(
    '/web/js/datepicker/datepicker.js',
    ['depends' => [yii\web\JqueryAsset::className()]]
);
$this->registerJsFile(
    '/web/js/books.js',
    ['depends' => [yii\web\JqueryAsset::className()]]
);

?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <form action="<?=Url::toRoute('/books/index')?>" method="GET">
        <div class="form-inline form-group">
            <select name="author" class="form-control">
                <option value="0">Все авторы</option>
                <?
                foreach ( $aAuthors as $oAuthor ) {
                ?>
                    <option value="<?=$oAuthor->id?>"><?=$oAuthor->firstname?> <?=$oAuthor->lastname?></option>
                <?
                }
                ?>
            </select>

            <div class="form-group">
                <input type="text" class="form-control" name="book-name" id="book-name" placeholder="Название книги">
            </div>
        </div>

        <div class="form-inline form-group pull-left">
            Дата выхода книги:
            <div class="form-group">
                <input type="text" class="form-control datepicker" name="date-from" id="date-from" placeholder="31/12/2014">
            </div>

            до

            <div class="form-group">
                <input type="text" class="form-control datepicker" name="date-to" id="date-to" placeholder="31/12/2015">
            </div>
        </div>

        <button type="submit" class="btn btn-primary pull-right">искать</button>
    </form>

    <?
        if ( !count($aBooks) ) {
    ?>
        <h2 class="clearfix">По Вашему запросу книги не найдены.</h2>
    <?
        } else {
    ?>
    <table class="table table-striped table-bordered">
        <thead>
        <tr class="active text-center">
            <td><a href="<?=$sUrlToSort?>&order-by=id&order=<? if ( $sOrderBy == 'id' && $sOrder == 'desc' ) { echo 'asc'; } else { echo 'desc'; }?>"><span class="glyphicon glyphicon-triangle-<? if ( $sOrderBy == 'id' && $sOrder == 'desc' ) { echo 'bottom'; } else { echo 'top'; }?>" aria-hidden="true"></span></a> ID</td>
            <td><a href="<?=$sUrlToSort?>&order-by=name&order=<? if ( $sOrderBy == 'name' && $sOrder == 'desc' ) { echo 'asc'; } else { echo 'desc'; }?>"><span class="glyphicon glyphicon-triangle-<? if ( $sOrderBy == 'name' && $sOrder == 'desc' ) { echo 'bottom'; } else { echo 'top'; }?>" aria-hidden="true"></span></a>  Название</td>
            <td>Превью</td>
            <td>Автор</td>
            <td><a href="<?=$sUrlToSort?>&order-by=date_create&order=<? if ( $sOrderBy == 'date_create' && $sOrder == 'desc' ) { echo 'asc'; } else { echo 'desc'; }?>"><span class="glyphicon glyphicon-triangle-<? if ( $sOrderBy == 'date_create' && $sOrder == 'desc' ) { echo 'bottom'; } else { echo 'top'; }?>" aria-hidden="true"></span></a> Дата выхода книги</td>
            <td><a href="<?=$sUrlToSort?>&order-by=date&order=<? if ( $sOrderBy == 'date' && $sOrder == 'desc' ) { echo 'asc'; } else { echo 'desc'; }?>"><span class="glyphicon glyphicon-triangle-<? if ( $sOrderBy == 'date' && $sOrder == 'desc' ) { echo 'bottom'; } else { echo 'top'; }?>" aria-hidden="true"></span></a> Дата обновления</td>
            <td colspan="3">Кнопки действий</td>
        </tr>
        </thead>
        <?
        foreach ( $aBooks as $oBook ) {
            ?>
            <tr>
                <td><?=$oBook->id?></td>
                <td><?=$oBook->name?></td>
                <td class="preview"><a href="<?=Yii::getAlias('@web')?><?=$oBook->preview?>" data-title="<?=$oBook->name?>" data-toggle="lightbox" title=""><?= Html::img( Yii::getAlias('@web') . $oBook->preview);?></a></td>
                <td><?=$oBook->author->firstname?> <?=$oBook->author->lastname?></td>
                <td><?=date( 'd M Y', strtotime( $oBook->date ))?></td>
                <td><?=date( 'd.m.Y', strtotime( $oBook->date_update) )?></td>
                <td><a href="<?=Url::to(['/books/edit', 'id' => $oBook->id]);?>" class="edit" title="">[ред]</a></td>
                <td><a href="#" data-id="<?=$oBook->id?>" class="info" title="">[просм]</a></td>
                <td><a href="<?=Url::to(['/books/delete', 'id' => $oBook->id]);?>" class="remove" title="">[удал]</a></td>
            </tr>
        <?
        }
        ?>
    </table>
    <?
        }
    ?>
</div>

<div class="modal fade" id="book-info" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Информация о книге</h4>
            </div>
            <div class="modal-body clearfix">
                <div class="image pull-left">
                    <img src="" title="" alt="">
                </div>
                <div class="info pull-right">
                    <div class="name"></div>
                    <div class="author"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
