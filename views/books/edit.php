<?php

use yii\helpers\Html;

$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile("/web/js/datepicker/datepicker.css");
$this->registerJsFile(
    '/web/js/datepicker/datepicker.js',
    ['depends' => [yii\web\JqueryAsset::className()]]
);
$this->registerJsFile(
    '/web/js/edit-book.js',
    ['depends' => [yii\web\JqueryAsset::className()]]
);

?>

<div class="form">
    <?php echo Html::beginForm(); ?>

    <?php echo Html::errorSummary($model); ?>

    <div class="row form-group">
        <?php echo Html::activeLabel($model,'name'); ?>
        <?php echo Html::activeTextInput($model,'name', ['class'=>'form-control']); ?>
    </div>

    <div class="row form-group">
        <?php echo Html::activeLabel($model,'date'); ?>
        <?php echo Html::activeTextInput($model,'date', ['class' => 'datepicker form-control']); ?>
    </div>

    <div class="row submit">
        <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']); ?>
    </div>

    <?php echo Html::endForm(); ?>
</div><!-- form -->