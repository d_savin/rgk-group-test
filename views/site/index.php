<?php

use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Тестовое задание для RGK GROUP';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Добро пожаловать!</h1>

        <?
            if ( Yii::$app->user->isGuest ) {
        ?>
                <p class="lead">Для дальнейшей работы вы должны <a href="<?=Url::toRoute('/site/login')?>" title="">авторизоваться</a>.</p>
        <?
            } else {
        ?>
                <p class="lead">Для дальнейшей работы выберите пункт меню.</p>
        <?
            }
        ?>
    </div>

    <div class="body-content">

    </div>
</div>
