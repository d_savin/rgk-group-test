<?php

namespace tests\codeception\unit\fixtures;

use yii\test\ActiveFixture;

class BooksFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Books';
}