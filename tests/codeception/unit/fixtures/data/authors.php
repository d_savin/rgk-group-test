<?php

return [
    [
        'id' => 1,
        'firstname' => 'Михаил',
        'lastname' => 'Булгаков',
    ],
    [
        'id' => 2,
        'firstname' => 'Эрих Мария',
        'lastname' => 'Ремарк',
    ],
    [
        'id' => 3,
        'firstname' => 'Джоан',
        'lastname' => 'Роулинг',
    ],
    [
        'id' => 4,
        'firstname' => 'Рэй',
        'lastname' => 'Брэдбери',
    ],
];