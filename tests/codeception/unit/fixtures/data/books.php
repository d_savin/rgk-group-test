<?php

return [
    [
        'name' => 'Мастер и Маргарита',
        'preview' => '/upload/Mihail_Bulgakov__Master_i_Margarita.jpeg',
        'date' => '2001-01-01 00:00:00',
        'author_id' => 1,
    ],
    [
        'name' => 'Морфий: Рассказы, повесть',
        'preview' => '/upload/Mihail_Bulgakov__Morfij_Rasskazy_povest.jpg',
        'date' => '2011-05-01 00:00:00',
        'author_id' => 1,
    ],
    [
        'name' => 'Собачье сердце',
        'preview' => '/upload/Mihail_Bulgakov__Sobache_serdtse.jpeg',
        'date' => '2001-08-01 00:00:00',
        'author_id' => 1,
    ],
    [
        'name' => 'Три товарища',
        'preview' => '/upload/Erih_Mariya_Remark__Tri_tovarischa.jpeg',
        'date' => '2010-04-01 00:00:00',
        'author_id' => 2,
    ],
    [
        'name' => 'На Западном фронте без перемен',
        'preview' => '/upload/Erih_Mariya_Remark__Na_Zapadnom_fronte_bez_peremen.jpeg',
        'date' => '2001-08-01 00:00:00',
        'author_id' => 2,
    ],
    [
        'name' => 'Триумфальная арка',
        'preview' => '/upload/Erih_Mariya_Remark__Triumfalnaya_arka.jpeg',
        'date' => '2014-01-01 00:00:00',
        'author_id' => 2,
    ],
    [
        'name' => 'Гарри Поттер и философский камень',
        'preview' => '/upload/Dzhoan_K._Roling__Garri_Potter_i_filosofskij_kamen.jpeg',
        'date' => '2006-04-01 00:00:00',
        'author_id' => 3,
    ],
    [
        'name' => 'Гарри Поттер и Тайная комната',
        'preview' => '/upload/Dzh._K._Roling__Garri_Potter_i_Tajnaya_komnata.jpeg',
        'date' => '2006-05-01 00:00:00',
        'author_id' => 3,
    ],
    [
        'name' => 'Гарри Поттер и узник Азкабана',
        'preview' => '/upload/Dzh._K._Roling__Garri_Potter_i_uznik_Azkabana.jpeg',
        'date' => '2007-01-01 00:00:00',
        'author_id' => 3,
    ],
    [
        'name' => 'О скитаньях вечных и о Земле',
        'preview' => '/upload/__O_skitanyah_vechnyh_i_o_Zemle.jpeg',
        'date' => '2012-05-01 00:00:00',
        'author_id' => 4,
    ],
    [
        'name' => '451° по Фаренгейту',
        'preview' => '/upload/Rej_Bredberi__451_po_Farengejtu.jpeg',
        'date' => '2010-09-11 00:00:00',
        'author_id' => 4,
    ],
    [
        'name' => 'Марсианские хроники',
        'preview' => '/upload/Rej_Bredberi__Marsianskie_hroniki.jpeg',
        'date' => '2011-11-12 00:00:00',
        'author_id' => 4,
    ],
];