<?php

namespace tests\codeception\unit\fixtures;

use yii\test\ActiveFixture;

class AuthorsFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Authors';
}