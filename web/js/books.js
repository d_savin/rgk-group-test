$(document).ready(function () {
    $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox({
            loadingMessage: 'Загрузка...'
        });
    });

    $('.datepicker').datepicker({
        format: 'mm/dd/yyyy',
    });

    $('.info').on('click', function ( event ) {
        event.preventDefault();

        var id = $(this).data('id');
        $.ajax({
            url: '/web/books/info/' + id,
            type: 'post',
            success: function(data) {
                console.log(data);

                $('#book-info .name').text( data.name );
                $('#book-info .author').text( data.author );
                $('#book-info .image img').attr( 'src', data.preview );

                $('#book-info').modal('show');
            },
            error: function () {
                alert('Error loading info');
            }
        });
    });
});