<?php

namespace app\controllers;

use app\models\Books;
use app\models\Authors;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Request;

class BooksController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'edit', 'delete', 'info'],
                'rules' => [
                    [
                        'actions' => ['index', 'edit', 'delete', 'info'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Действие по умолчанию
     *
     * @return string
     */
    public function actionIndex()
    {
        Url::remember();

        $sUrlToSort = Url::toRoute('/books/index') . '?';

        // Фильтры
        $iAuthorId = Yii::$app->getRequest()->getQueryParam('author');
        $sBookName = Yii::$app->getRequest()->getQueryParam('book-name');
        $sDateFrom = Yii::$app->getRequest()->getQueryParam('date-from');
        $sDateTo = Yii::$app->getRequest()->getQueryParam('date-to');
        $sOrderBy = Yii::$app->getRequest()->getQueryParam('order-by');
        $sOrder = Yii::$app->getRequest()->getQueryParam('order');

        $aBooks = Books::find();
        if ( $iAuthorId ) {
            $aBooks->andWhere(['author_id' => $iAuthorId]);
            $sUrlToSort .= 'author=' . $iAuthorId;
        }

        if ( !empty($sBookName) ) {
            $aBooks->andWhere(['like', 'name', $sBookName]);
            $sUrlToSort .= 'book-name=' . urlencode($sBookName);
        }

        if ( !empty( $sDateFrom ) ) {
            $aBooks->andWhere(['>=', 'date', date('Y-m-d', strtotime($sDateFrom))]);
            $sUrlToSort .= 'date-from=' . urlencode($sDateFrom);
        }

        if ( !empty( $sDateTo ) ) {
            $aBooks->andWhere(['<=', 'date', date('Y-m-d', strtotime($sDateTo))]);
            $sUrlToSort .= 'date-to=' . urlencode($sDateTo);
        }

        if ( !empty( $sOrderBy ) ) {
            $aBooks->orderBy( [ $sOrderBy => $sOrder == 'desc' ? SORT_DESC : SORT_ASC ] );
        }

        $aBooks = $aBooks->all();
        $aAuthors = Authors::find()->all();
        $this->view->title = 'Книги';

        return $this->render('index', array(
            'aBooks' => $aBooks,
            'aAuthors' => $aAuthors,
            'sUrlToSort' => $sUrlToSort,
            'sOrderBy' => $sOrderBy,
            'sOrder' => $sOrder
        ));
    }

    /**
     * Удаление книги
     *
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDelete ( $id ) {
        $oBook = Books::findOne( $id );
        if ( $oBook === null ) {
            throw new NotFoundHttpException;
        }

        $sImagePath = Yii::getAlias('@webroot') . $oBook->preview;

        if ( file_exists( $sImagePath ) ) {
            @unlink( $sImagePath );
        }

        $oBook->delete();

        return $this->redirect('/web/books/index');
    }

    /**
     * Информация о книге
     *
     * @param $id
     * @return array
     */
    public function actionInfo ( $id ) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $oBook = Books::findOne( $id );

        $aResponse = array( 'error' => false );
        if ( $oBook === null ) {
            $aResponse['error'] = true;
        } else {
            $aResponse['name'] = $oBook->name;
            $aResponse['author'] = $oBook->author->firstname . ' ' . $oBook->author->lastname;
            $aResponse['preview'] = Yii::getAlias('@web') . $oBook->preview;
        }

        return $aResponse;
    }

    /**
     * Редактирование книги
     *
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionEdit ( $id ) {
        $this->view->title = 'Редактирование книги';

        $oBook = Books::findOne( $id );
        if ( Yii::$app->getRequest()->isPost ) {
            $oBook->load( $_POST );
            $oBook->date_update = date( 'Y-m-d H:i:s' );
            $oBook->date = date( 'Y-m-d H:i:s', strtotime( $_POST['Books']['date'] ) );

            $oBook->save();

            return $this->redirect(Url::previous());
        }

        return $this->render('edit', array(
            'model' => $oBook
        ));
    }
}
